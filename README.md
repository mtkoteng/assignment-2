# Assignment 2 - File system manager
## The task
The task was to create an console application, which offer different options interacting with different files, and to log these interactions.
## The solution
I choosed to create to different applications, one console and one with Java Swing. The Java Swing app does the same as the console, except the logging functionality.

**For starting the console application: go to /out and then run "java -jar Assignment2.jar" OR run the java/console/Main.java file**

**For starting the GUI application: run the java/gui/MainGUI.java file**
### Listing all the files
For listing the files, the Java File package was used. It checks if it is in a folder, and displays the files in the given folder.
### Listing the files of a specific file-type
The Java File package was used to collect the files. Based on the specific type (the ending of the file), the files were placed into an ArrayList. 
### Interaction with Dracula.txt
To read information from the Dracula file, Java File, FileInputStream, BufferedReader and FileReader where used. To be able to display the title, Java File was used. With only care about the text before the punctuation, the title was displayed. FileInputStream was used to calculate the size of the file, by reading each bytes, and adding them together. BufferedReader was used for calculating the number of lines in the application, by reading each line, and then adding them together. BufferedReader was also used to search for word in the text. It ignores all other signs than letters, and also ignores if it is capital letters or not. Based on the match, it calculates the number of the word in the text.
### Logging the interaction
The interaction which the user does in the application, is logged into an ArrayList, which is written to Log.txt. Each log have a timestamp, and the log for function excecution, has duration of execution logged. The project uses FileWriter to write the ArrayList to the log file. NB! The GUI-application does not offer all the logging functionality.
### Error handling
For the parts of the application where you read and write to a file, try/catch is used to check if it is possible to do the actions to the files. For listing the files, if else is used to check if you stand in a folder and if it is files there. For the input, if else is used to check if the input is valid (if it is a number whithin the limit).
