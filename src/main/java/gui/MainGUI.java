package main.java.gui;
import main.java.Logger;
import main.java.ReadFiles;

import java.util.ArrayList;

import static javax.swing.JOptionPane.*;

/*
* This is a voluntary part. It creates a GUI with the usage of Java Swing.
* The class lists up actions in buttons, which is used to navigate through the application.
* Same functionality as in the console menu, but do not have all the logging functionality.
*/
public class MainGUI {
    String[] options = {"All files", "Search for format", "Dracula", "Close"};
    String[] fileTypes = {"txt", "jpeg", "png", "jpg", "jfif", "Cancel"};

    //Displays the choices for the main menu
    private int mainChoices(){
        return showOptionDialog(null, "Welcome to this application!",
                "Assignment 2", DEFAULT_OPTION, INFORMATION_MESSAGE, null, options, options[3]);
    }

    //Displays the choices for the menu for selecting file types
    private int fileTypeChoice(){
        return showOptionDialog(null, "Choose a file type",
                "Assignment 2", DEFAULT_OPTION, INFORMATION_MESSAGE, null, fileTypes, fileTypes[5]);
    }

    //Prints out an ArrayList with the files with the spesific file-type
    private void printTypesOfFile(int option, ReadFiles rfl){
        String format = fileTypes[option];
        ArrayList<String> filesFormat = rfl.getFilesByExtension(format);
        String result = "";
        for (int i = 0; i < filesFormat.size(); i++) {
            result += (i+1) + ": " + filesFormat.get(i) + "\n";
        }
        showMessageDialog(null, result);
    }

    //Handles the file-type menu. Some redundancy
    public void fileTypeMenu(ReadFiles rfl){
        int option = fileTypeChoice();
        while (option != 5){
            switch (option){
                case 0:
                    printTypesOfFile(option, rfl);
                    option = fileTypeChoice();
                    break;
                case 1:
                    printTypesOfFile(option, rfl);
                    option = fileTypeChoice();
                    break;
                case 2:
                    printTypesOfFile(option, rfl);
                    option = fileTypeChoice();
                    break;
                case 3:
                    printTypesOfFile(option, rfl);
                    option = fileTypeChoice();
                    break;
                case 4:
                    printTypesOfFile(option, rfl);
                    option = fileTypeChoice();
                    break;
            }
        }
    }

    //Handles the dracula menu. Offers the possibillity to search for words in Dracula.txt
    public void draculaMenu(ReadFiles rfl, Logger log){
        log.log("Entered dracula menu");
        String menuText = "Title: " + rfl.fileName() + "\nSize: " + rfl.fileSize() + " bytes\nNumber of lines "+
                rfl.numOfLines() + "\n"+ "Search for a word in Dracula";
        String input = showInputDialog(menuText);
        while (input != null){
            if (!input.equals("")){
                log.log("The user did a search. " + rfl.searchInFile(input));
                showMessageDialog(null, rfl.searchInFile(input));
                input = showInputDialog(menuText);
            } else {
                showMessageDialog(null, rfl.searchInFile(input));
                input = showInputDialog(menuText);
            }
        }
        log.writeToFile();
    }

    public static void main(String[] args) {
        MainGUI gui = new MainGUI();
        ReadFiles rfl = new ReadFiles();
        Logger logger = new Logger();
        int choice = gui.mainChoices();
        while (choice != 3){
            switch (choice){
                case 0:
                    String[] fileNames = rfl.allFiles();
                    showMessageDialog(null, fileNames);
                    choice = gui.mainChoices();
                    break;
                case 1:
                    gui.fileTypeMenu(rfl);
                    choice = gui.mainChoices();
                    break;
                case 2:
                    System.out.println("Loading... Wait some seconds");
                    gui.draculaMenu(rfl, logger);
                    choice = gui.mainChoices();
                    break;
                default:
                    System.out.println("Not a possibillity");
            }
        }
        System.out.println("quited");
        showMessageDialog(null, "Your actions is written to Log.txt \nGoodbye!");
    }
}
