package main.java;

import java.io.*;
import java.util.ArrayList;

/*
* This class handles the tasks from the Assignment regarding the file manipulation
* It reads the files in the directory, and gathering relevant information about the files.
*/
public class ReadFiles {
    File folder = new File("src/main/resources");
    int numOfFiles = 13;
    File[] listOfFiles = new File[numOfFiles];

    //Helping method for checking if we are in a folder/directory
    private boolean isFolder(){
        if (folder.isDirectory()){
            listOfFiles = folder.listFiles();
            return true;
        }
        throw new NullPointerException("It is not a folder!");
    }

    //This method displays the files in a folder, if we are in one.
    //It checks if the files in the directory, really is a file, and then adds the name to the new array
    public String[] allFiles(){
        if (isFolder()){
            String[] fileNames = new String[listOfFiles.length];
            System.out.println("Files:");
            for (int i = 0; i < listOfFiles.length; i++){
                if (listOfFiles[i].isFile()){
                    fileNames[i] = listOfFiles[i].getName();
                } else {
                    System.out.println("Did not find any files!");
                }
            }
            return fileNames;
        }
        return null;
    }

    //Creates an ArrayList with the files with the specific extension. Creates the opportunity to search on a format.
    //Since the extention is the last part and the user does not write the search self, I used the endWith() method.
    public ArrayList<String> getFilesByExtension(String extension){
        if (isFolder()){
            String filename;
            ArrayList<String> newFiles = new ArrayList<>();
            for (int i = 0; i < listOfFiles.length; i++){
                filename = listOfFiles[i].getName();
                if (filename.endsWith(extension)){
                    newFiles.add(filename);
                }
            }
            return newFiles;
        }
        return null;
    }

    String pathFile = "src/main/resources/Dracula.txt";
    //Finds the name of the file, by only displaying the part before the punctuation
    public String fileName(){
        File file = new File(pathFile);
        String titleWithFormat = "";
        if (file.isFile()){
            titleWithFormat = file.getName();
            String[] title = titleWithFormat.split("[\\p{Punct}\\s]+");
            return title[0];
        }
        return titleWithFormat;
    }

    //Calculates the file size with the usage of FileInputStream
    public int fileSize(){
        int numOfBytes = 0;
        try (FileInputStream fileInputStream = new FileInputStream(pathFile)) {
            int data = fileInputStream.read();
            while (data != -1){
                numOfBytes++;
                data = fileInputStream.read();
            }

        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }
        return numOfBytes;
    }

    //Calculates the number of lines in the file with BufferedReader and FileReader
    public int numOfLines(){
        int numLines = 0;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(pathFile))){
                while ((bufferedReader.readLine()) != null){
                numLines++;
            }
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }
        return numLines;
    }

    //Searches for a word in the file, and calculates the number of times the word is written in the file.
    //Ignores spaces, big letters and other signs
    public String searchInFile(String search){
        String answer = "";
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(pathFile))) {
            int numOfSearch = 0;
            String word;
            String[] words;
            while ((word=bufferedReader.readLine()) != null && search != null && !search.equals("")){
                words = word.split(" ");
                search = search.toLowerCase();
                search = search.replaceAll("[\\W]", "");
                for (int i = 0; i < words.length; i++) {
                    //lower cases all the words and replaces all signs that is not a letter
                    words[i] = words[i].toLowerCase();
                    word = words[i].replaceAll("[\\W]", "");
                    if (word.equals(search)){
                        numOfSearch++;
                    }
                }
            }
            if (numOfSearch == 0){
                answer = "The word ''"+ search + "'' does not exists in this file.";
            } else {
                answer = "The word ''" + search + "'' exists, and has " + numOfSearch + " numbers of hits in the file.";
            }
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }
        return answer;
    }
}
