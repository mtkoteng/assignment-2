package main.java;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/*
* The logger takes care of the logs in the application. Each time you do a action a string
* with log is added to an ArrayList. The ArrayList is written to Log.txt when closing the application.
*/
public class Logger {
    ArrayList<String> messages = new ArrayList<>();

    //Logs a message to the ArrayList, with a timestamp
    public ArrayList<String> log(String message){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        messages.add(sdf.format(timestamp) + ": " + message);
        return messages;
    }

    //Writes the ArrayList to the Log.txt file.
    public void writeToFile(){
        try (FileWriter fileWriter = new FileWriter("src/main/resources/Log.txt")){
            for (String str : messages) {
                fileWriter.write(str + "\n");
            }
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }
}
