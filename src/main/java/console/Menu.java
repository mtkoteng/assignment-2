package main.java.console;

import main.java.Logger;
import main.java.ReadFiles;

import java.util.ArrayList;
import java.util.Scanner;

/*
* Class for handling the different menus in the console application
* */
public class Menu {
    private ReadFiles readFiles = new ReadFiles();
    private ArrayList<String> logList = new ArrayList<>();

    public void mainMenu(){
        System.out.println("1: List all the files");
        System.out.println("2: List files of specific format");
        System.out.println("3: Info about Dracula");
        System.out.println("4 (or any other number): Quit application");
        System.out.println("Enter your choice: ");
    }

    //The menu when entering the part where you can see files based on what format you choose.
    public void fileFormMenu(){
        System.out.println("1: txt");
        System.out.println("2: jpeg");
        System.out.println("3: jpg");
        System.out.println("4: png");
        System.out.println("5: jfif");
        System.out.println("6 (or any other signs): Back to main menu");
        System.out.println("Make a choice: ");
    }

    //The menu-handler when entering the part where you can see files based on what format you choose.
    public void fileFormatMenu(Logger logger){
        logger.log("Selected option 2 in main menu");
        fileFormMenu();
        Scanner sc2 = new Scanner(System.in);
        int format = isNumber(sc2);
        String type = "";
        ArrayList<String> filesFormat = new ArrayList<>();
        while (format > 0 && format < 6){
            switch (format){
                case 1:
                    type = "txt";
                    long startTime = System.currentTimeMillis();
                    filesFormat = readFiles.getFilesByExtension(type);
                    for(int i = 0; i < filesFormat.size(); i++){
                        System.out.println((i+1) + ": " + filesFormat.get(i));
                    }
                    long endTime = System.currentTimeMillis();
                    logger.log("Choosed to display the files on the " + type + " format. The function took " + (endTime-startTime) + "ms to execute.");
                    System.out.println("-------------------");
                    fileFormMenu();
                    break;
                case 2:
                    type = "jpeg";
                    long startTime2 = System.currentTimeMillis();
                    filesFormat = readFiles.getFilesByExtension(type);
                    for(int i = 0; i < filesFormat.size(); i++){
                        System.out.println((i+1) + ": " + filesFormat.get(i));
                    }
                    long endTime2 = System.currentTimeMillis();
                    logger.log("Choosed to display the files on the " + type + " format. The function took " + (endTime2-startTime2) + "ms to execute.");
                    System.out.println("-------------------");
                    fileFormMenu();
                    break;
                case 3:
                    type = "jpg";
                    long startTime3 = System.currentTimeMillis();
                    filesFormat = readFiles.getFilesByExtension(type);
                    for(int i = 0; i < filesFormat.size(); i++){
                        System.out.println((i+1) + ": " + filesFormat.get(i));
                    }
                    long endTime3 = System.currentTimeMillis();
                    logger.log("Choosed to display the files on the " + type + " format. The function took " + (endTime3-startTime3) + "ms to execute.");
                    System.out.println("-------------------");
                    fileFormMenu();
                    break;
                case 4:
                    type = "png";
                    long startTime4 = System.currentTimeMillis();
                    filesFormat = readFiles.getFilesByExtension(type);
                    for(int i = 0; i < filesFormat.size(); i++){
                        System.out.println((i+1) + ": " + filesFormat.get(i));
                    }
                    long endTime4 = System.currentTimeMillis();
                    logger.log("Choosed to display the files on the " + type + " format. The function took " + (endTime4-startTime4) + "ms to execute.");
                    System.out.println("-------------------");
                    fileFormMenu();
                    break;
                case 5:
                    type = "jfif";
                    long startTime5 = System.currentTimeMillis();
                    filesFormat = readFiles.getFilesByExtension(type);
                    for(int i = 0; i < filesFormat.size(); i++){
                        System.out.println((i+1) + ": " + filesFormat.get(i));
                    }
                    long endTime5 = System.currentTimeMillis();
                    logger.log("Choosed to display the files on the " + type + " format. The function took " + (endTime5-startTime5) + "ms to execute.");
                    System.out.println("-------------------");
                    fileFormMenu();
                    break;
            }
            format = sc2.nextInt();
            logger.writeToFile();
        }
        mainMenu();
    }

    //The menu when entering the Dracula part of the application
    public void draculaMenu(Logger logger){
        logger.log("Selected option 3 in main menu");
        long startTimeLoading = System.currentTimeMillis();
        String fileName = readFiles.fileName();
        int fileSize = readFiles.fileSize();
        int numOfLines = readFiles.numOfLines();
        System.out.println("Title: " + fileName + "\nFile size: " + fileSize + " bytes \nNumber of lines: " + numOfLines);
        long endTimeLoading = System.currentTimeMillis();
        logger.log("Loading the data from Dracula, tooked " + (endTimeLoading-startTimeLoading) + "ms to execute");
        System.out.println("What word do you want to find? Press 1 for main menu");
        Scanner sc3 = new Scanner(System.in);
        String search = sc3.next();
        while (!(search.equals("1"))){
            long startTimeSearch = System.currentTimeMillis();
            String answer = readFiles.searchInFile(search);
            System.out.println(answer);
            long endTimeSearch = System.currentTimeMillis();
            System.out.println("---------------------------------------");
            System.out.println("Continue searching, or press 1 for main menu");
            logger.log("The user did a search. " + answer + " The function took " + (endTimeSearch-startTimeSearch) + "ms to execute");
            search = sc3.next();
        }
        logger.writeToFile();
        System.out.println("---------------------------------------");
        mainMenu();
    }

    //Checking if the input from the scanner is a number. If not, 0 will be returned.
    public int isNumber(Scanner sc){
        int num;
        if (sc.hasNextInt()){
            num = sc.nextInt();
            return num;
        }
        num = 0;
        System.out.println("The input was invalid! Try again.");
        sc.next();
        return num;
    }
}
