package main.java.console;
import main.java.Logger;
import main.java.ReadFiles;

import java.util.Scanner;

/*
* This class runs the console application
*/
public class Main {

    public static void main(String[] args) {
        ReadFiles readFiles = new ReadFiles();
        Logger logger = new Logger();
        Menu menu = new Menu();

        System.out.println("Hello There!");
        menu.mainMenu();
        Scanner scanner = new Scanner(System.in);

        int choice = menu.isNumber(scanner);

        while (choice < 4 && choice > -1){
            switch (choice){
                case 0:
                    logger.log("User wrote invalid input!");
                    logger.writeToFile();
                    menu.mainMenu();
                    choice = menu.isNumber(scanner);
                    break;
                case 1:
                    long startTime = System.currentTimeMillis();
                    System.out.println("---------------------------------------");
                    String[] fileNames = readFiles.allFiles();
                    for (int i = 0; i < fileNames.length; i++) {
                        System.out.println((i+1) + ": " + fileNames[i]);
                    }
                    System.out.println("---------------------------------------");
                    long endTime = System.currentTimeMillis();
                    logger.log("Selected option 1 in main menu. It tooked " + (endTime-startTime) + "ms to execute");
                    logger.writeToFile();
                    menu.mainMenu();
                    choice = menu.isNumber(scanner);
                    break;
                case 2:
                    System.out.println("---------------------------------------");
                    System.out.println("Here you kan list the files based on format. Write the format:");
                    menu.fileFormatMenu(logger);
                    choice = menu.isNumber(scanner);
                    break;
                case 3:
                    System.out.println("---------------------------------------");
                    System.out.println("Info about Dracula.txt: (wait a few seconds)");
                    menu.draculaMenu(logger);
                    choice = menu.isNumber(scanner);
                    break;
                default:
                    System.out.println("This cannot be choosed");
            }
        }
        System.out.println("Your actions is written to Log.txt");
        System.out.println("Goodbye!");
        logger.log("Exits the application");
        logger.writeToFile();
    }
}
